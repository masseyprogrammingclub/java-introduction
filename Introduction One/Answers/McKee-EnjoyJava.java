import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class EnjoyJava {
	
	public static int[] numbersA = new int[5];
	public static int[] numbersB = {1, 2, 3, 4, 5};
	public int value = 0;
	/*
	 * Constructor
	 */
	public EnjoyJava(int v) {
		value = v;
	}
	
	/*
	 * Overloaded Constructor
	 */
	public EnjoyJava(int v, boolean bo) {
		value = v;
	}
	
	/*
	 * Runs 
	 */
	public static void main(String [] args) {
		int[] takeNumbersB = numbersB;
		System.out.println(takeNumbersB.toString());
		equalsComp();
		listExample(takeNumbersB);
		int [] ans = fibSequence();
		for (int x : ans) {
			System.out.print(x + " ");
		}
		System.out.println("\n");
		System.out.println(average(takeNumbersB));	
	}
	
	/*
	 * Comparing == and .equals
	 */
	public static void equalsComp() {
		String sA = "String A";
		String sB = "String A";
		System.out.println(sA.equals(sB));
		System.out.println(sB == sA);
	}
	
	public static void listExample(int[] passedList) {
		// Iterator 
		for (int i  : passedList) {
			System.out.print(i + " ");
		}
		System.out.println("\n");
		for (int i =0; i<passedList.length; i++) {
			System.out.print(passedList[i] + " ");
		}
	}
	
	public static int[] fibSequence() {
		Scanner scan = new Scanner(System.in);
		int limit = scan.nextInt();
		int [] sequence = new int[limit];
		sequence[0] = 1;
		sequence[1] = 1;
		
		for (int i = 2; i<limit; i++) {
			sequence[i] = sequence[i-1] + sequence[i-2];
		}
		return sequence;
	}
	
	public static int average(int [] numbers) {
		int sum = 0;
		int len = numbers.length;
		for(int i=0; i < len; i++) {
			sum += numbers[i];
		}
		return sum/len;
	}
	
	public static void arrayListExample() {
		List<String> arrayListEx = new ArrayList<String>();
		arrayListEx.add("1");
		arrayListEx.add("2");
		arrayListEx.add("3");
		
		arrayListEx.remove(0);
		arrayListEx.remove("3");
	}
	
	/*
	 * Iterators
	 */
	
}
